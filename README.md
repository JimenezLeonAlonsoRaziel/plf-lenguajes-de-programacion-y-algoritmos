#Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startmindmap
+[#lightblue] Programando ordenadores en los 80 y ahora
++[#lightgreen] 80s
+++[#lightyellow] Ordenadores en los 80 
++++[#lightpink] Buen funcionamiento
++++[#lightpink] Lenguaje de bajo nivel
+++++_ era
++++++[#yellow] Ensamblador
++++[#lightpink] Eficiencia
+++++_ aprovechaba
++++++[#yellow] Recursos
++++++[#yellow] Memoria
++++[#lightpink] Mayor potencia
++++[#lightpink] Arquitectura conocida
++[#lightgreen] Ahora
+++[#lightgray] Eficiencia
++++_ tiene
+++++[#yellow] Recursos de sobra
+++[#lightgray] Mejor interfaz
+++[#lightgray] Problemáticas
++++_ requiere
+++++[#yellow] Mejor hardware y software
+++[#lightgray] Funciones
++++_ por ejemplo
+++++[#yellow] Poca eficiencia
+++++[#yellow] Propenso a errores
+++++[#yellow] Funcionamiento irregular
+++[#lightgray] Arquitectura menos robusta pero reciente
+++[#lightgray] Lenguajes
++++_ como
+++++[#yellow] Java
+++++[#yellow] C++
+++++[#yellow] C
++[#lightgreen] Leyes
+++ Ley de Wirth
++++_ menciona que
+++++[#yellow] Se puede generar una CPU el doble de rápida cada 18 meses
+++ Ley de Moore
++++_ menciona que
+++++[#yellow] El software se vuelve el doble de lento
@endmindmap
```


#Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
+[#lightgreen] Historia de los algoritmos y lenguajes de programación
++[#orange] Algoritmos
+++[#yellow] Historia
++++[#lightgray] 3000 a.C
+++++_ eran
++++++[#lightblue] Tablillas de arcilla
++++[#lightgray] Siglo XVII
+++++_ eran
++++++[#lightblue] Primeras calculadoras mecánicas
++++[#lightgray] Siglo XIX
+++++_ eran
++++++[#lightblue] Primeras máquinas programables
+++[#yellow] Tipos
++++_ pueden ser
+++++[#lightgray] Razonables
++++++[#lightblue] Crecimiento lento en el tiempo de ejecución en aumento de problemas
+++++[#lightgray] No razonables
++++++[#lightblue] Duplicidad de tiempo en datos extra al problema
+++[#yellow] Origenes
++++_ se dieron
+++++[#lightgray] Antes de la computadora
+++++[#lightgray] En las calculadoras
++[#orange] Generaciones
+++[#yellow] Primera
++++[#lightgray] Lenguaje máquina
+++[#yellow] Segunda
++++[#lightgray] Año 1940 (ensamblador)
+++[#yellow] Tercera
++++[#lightgray] Año 1957 (Fortran)
++++[#lightgray] Año 1960 (COBOL)
++++[#lightgray] Año 1963 (PL/I)
++++[#lightgray] Año 1964 (BASIC y LOGO)
++++[#lightgray] Año 1970 (PASCAL)
+++[#yellow] Cuarta
++++[#lightgray] Año 1972 (C)
++++[#lightgray] Año 1979 (C++)
++++[#lightgray] Año 1991 (HTML, PYTHON, VB)
++++[#lightgray] Año 1995 (JAVA, PHP, JS)
++[#orange] Lenguajes de programación
+++[#yellow] Paradigmas
++++_ son
+++++[#lightgray] Paradigma Imperativo (1959)
+++++[#lightgray] Paradigma Funcional (1960)
++++++[#lightblue] Trabaja con la idea de la simplificación
+++++[#lightgray] Lenguaje orientado a objetos (1968)
++++++[#lightblue] Organiza el diseño entorno a objetos y datos.
+++++[#lightgray] Paradigma Lógico (1971)
++++++[#lightblue] Procesos paralelos distribuidos
+++[#yellow] Lenguajes destacados
++++[#lightgray] Java
++++[#lightgray] C++
++++[#lightgray] Fortran
@endmindmap
```

#Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
+[#lightgreen] Evolucion de los lenguajes y paradigmas de programacion y tendencias actuales
++[#lightpink] Paradigmas
+++[#yellow] Distribuido
++++_ existe
+++++[#lightgray] Comunicación entre computadoras
++++++_ lenguajes
+++++++[#orange] Ada
+++++++[#orange] E
+++++++[#orange] Limbo
+++++++[#orange] Oz
+++[#yellow] Funcional
++++_ utiliza
+++++[#lightblue] Funciones matemáticas y recursividad
++++++_ lenguajes
+++++++[#white] F#
+++++++[#white] Haskell
+++++++[#white] Erlang
+++[#yellow] Lógico
++++_ retorna
+++++[#lightgray] true o false
++++++_ lenguajes
+++++++[#orange] Prolog
+++++++[#orange] Mercury
+++++++[#orange] ALF
+++[#yellow] Programación concurrente
++++_ existe
+++++[#lightblue] Paralelismo entre tareas
++++++_ lenguajes
+++++++[#white] Ada
+++++++[#white] Java
+++[#yellow] Programación orientada a objetos
++++_ consiste en
+++++[#lightgray] Abstracciones del mundo real
++++++_ lenguajes
+++++++[#orange] Java
+++++++[#orange] PHP
+++++++[#orange] JS
+++[#yellow] Programación orientada en aspectos
++++_ agrega
+++++[#lightblue] Capas independientes
++++++_ permiten
+++++++[#white] Legibilidad
+++++++[#white] Comodidad
++++_ requiere
+++++[#lightgray] Capa base dependiendo la necesidad del programa
+++[#yellow] Programación orientada en componentes
++++_ utiliza
+++++[#lightblue] Reutilización donde es un conjunto de componentes
++++++_ resuelve
+++++++[#orange] problemas, diálogos, cooperación, coordinación y negociación
++[#lightpink] Programación estructurada
+++_ es
++++ Programación por funciones o rutinas
+++++_ lenguajes
++++++[#white] Algol
++++++[#white] Pascal
++++++[#white] Ada
++++++[#white] Basic
++++++[#white] C
++++++[#white] Java
@endmindmap
```